import { Component, OnInit } from '@angular/core';
import { OrderLine } from 'src/Models/order-line';
import { Articles } from 'src/Models/articles';
import { Frames } from 'src/Models/frames';
import { Finish } from 'src/Models/finish';
import { ArticlesService } from 'src/Services/articles.service';
import { FramesService } from 'src/Services/frames.service';
import { Format } from 'src/Models/format';
import { FinishesService } from 'src/Services/finishes.service';
import { FormatsService } from 'src/Services/formats.service';
import { CartService } from 'src/Services/cart.service';
import { OrderService } from 'src/Services/order.service';
import { Order } from 'src/Models/order';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  private totalPrice:number;
  private orders:OrderLine[];
  private quantity:number;
  private commandStatus:string;

  constructor(private articlesService:ArticlesService, private framesService:FramesService, private finishesService:FinishesService, private formatsServices:FormatsService, private cartService:CartService, private orderService:OrderService){ }

  ngOnInit() {
    this.orders = new Array();
    this.orders = JSON.parse(localStorage.getItem("cart"));
    this.getTotalPrice();
    this.commandStatus = "en cours";
  }

  getTotalPrice(){
    this.totalPrice = 0;
    this.orders.forEach(order => {
      this.totalPrice += order.price * order.quantity;
    });
  }

  saveChange(orderLine:OrderLine){
    if (orderLine.quantity < 1){
      orderLine.quantity = 1;
    }
    else if(orderLine.quantity > orderLine.article.stock){
      orderLine.quantity = orderLine.article.stock;
    }
    this.getTotalPrice();
    localStorage.setItem("cart", JSON.stringify(this.orders))
  }

  deleteLine(orderLine:OrderLine){
    let index:number = this.orders.indexOf(orderLine);
    if (index !== -1){
      this.orders.splice(index, 1);
    }
    this.getTotalPrice();
    localStorage.setItem("cart", JSON.stringify(this.orders))
    this.cartService.cartChanged();
  }

  validateCommand(){
    console.log("validate");
    console.log(this.orders);
    this.orderService.addOrder(new Order(this.orders)).subscribe(
      order => this.commandStatus = "commande",
      error => this.commandStatus = "erreur"
    );
  }
}