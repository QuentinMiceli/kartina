import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})

export class HelpComponent implements OnInit {

  mailtoBase = "mailto:kartina@gmail.com?";

  cc:string = "";
  subject:string = "";
  body:string ="";
  
  constructor() {
  }

  ngOnInit() { }

  clickSend() {
    this.mailtoBase += "&cc="+this.cc+"&Subject="+this.subject+"&body="+this.body;
  }

}
