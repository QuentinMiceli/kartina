import { Component, OnInit } from '@angular/core';
import { Articles } from '../../Models/articles';
import { ArticlesService } from '../../Services/articles.service'
import $ from 'jquery';
import { ArticlesReturn } from '../../Models/articles-return'
import { ThrowStmt } from '@angular/compiler';
import { Filter } from '../../Models/filter';
import { ThemeService } from 'src/Services/theme.service';
import { TouchSequence } from 'selenium-webdriver';
import { cardAnimation } from '../route-animation';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss'],
  animations: [ cardAnimation ]
})

export class PhotosComponent implements OnInit {
  private images: ArticlesReturn;
  private orientation: string;
  private priceMax: number;
  private priceMin: number;
  private themeId: number;
  private page: number;
  private themes: any;
  private pages: number[];
  private numberOfPages: number;

  constructor(private articlesService: ArticlesService, private themesService: ThemeService) {
    this.images = new ArticlesReturn();
    this.images.articles = new Array();
    this.themes = new Array();
    this.orientation = null;
    this.priceMax = 0;
    this.priceMin = 0;
    this.themeId = 0;
    this.page = 1;
    this.pages = new Array();
    this.numberOfPages = 1;
  }

  ngOnInit() {
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    this.getArticles();

    this.themesService.getThemes().subscribe(
      (theme: Filter) => this.themes = theme,
      error => console.log(error.message))
  }

  getArticles(): void {
    this.images = new ArticlesReturn;
    this.articlesService.getArticles(this.orientation, this.priceMin, this.priceMax, this.themeId, this.page)
      .subscribe(
        images => this.images = images,
        error => console.log(error),
        () => {
          this.pages = new Array();
          this.numberOfPages = Math.ceil(this.images.numberOfArticles / 10)
          if (this.numberOfPages === 0)
            this.numberOfPages = 1;
          for (let i: number = 1; i <= this.numberOfPages; i++)
            this.pages.push(i);
        }
      )
  }

  setTheme(themeId: number): void {
    this.themeId = themeId;
    this.page = 1;
    this.getArticles();
  }

  setOrientation(orientation: string) {
    this.orientation = orientation;
    this.page = 1;
    this.getArticles();
  }

  setPrice(min: number, max: number) {
    this.priceMin = min;
    this.priceMax = max;
    this.page = 1;
    this.getArticles();
  }

  setPage(index: number) {
    this.page = index;
    this.getArticles();
    window.scrollTo(0, 0);
  }

  getPreviousPage(): void {
    this.page = this.page - 1;
    this.getArticles();
    window.scrollTo(0, 0);
  }

  getNextPage(): void {
    this.page = this.page + 1;
    this.getArticles();
    window.scrollTo(0, 0);
  }

  resetFilters(): void {
    this.orientation = null;
    this.themeId = 0;
    this.priceMax = 0;
    this.priceMin = 0;
    this.getArticles();
  }
}