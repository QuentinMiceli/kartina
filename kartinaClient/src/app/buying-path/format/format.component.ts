import { Component, OnInit } from '@angular/core';
import { FormatsService } from 'src/Services/formats.service';
import { Format } from 'src/Models/format';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-format',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss']
})
export class FormatComponent implements OnInit {

  private selection:number;
  private formats:Format[];
  private itemPrice:number;

  constructor(private formatsService:FormatsService, private cookies:CookieService) {
    this.formats = new Array();
    this.selection = 0;
  }

  ngOnInit() {
    this.formatsService.getFormats().subscribe(
      (formats:Format[]) => this.formats = formats,
      error => console.log(error.message),
    )
    this.itemPrice = parseFloat(sessionStorage.getItem("price"));
  }

  setSelection(id:number):void{
    this.selection = id;
    console.log(this.selection);
  }

  saveChoice():void{
    if(this.selection > 0)
      sessionStorage.setItem("format", this.selection.toString())
    this.formats.forEach(format => {
      if(format.id === this.selection)
        sessionStorage.setItem("priceWithFormat", (this.itemPrice * (format.percentagePriceChange / 100)).toString())
    })
  }

}
