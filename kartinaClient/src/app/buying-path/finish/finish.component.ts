import { Component, OnInit } from '@angular/core';
import { FinishesService } from 'src/Services/finishes.service';
import { Finish } from 'src/Models/finish';
import { CookieService } from 'ngx-cookie-service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {

  private finishes:Finish[];
  private selection:number;
  private itemPrice:number;

  private curentFormat:number;

  constructor(private finishesService:FinishesService, private cookieService:CookieService,private _location: Location) {
    this.selection = 0;
  }

  ngOnInit() {
    this.curentFormat = parseInt(sessionStorage.getItem("format"));

    this.finishesService.getFinishes().subscribe(
      finishes => this.finishes = finishes,
      error => console.log(error.message),
    )
    this.itemPrice = parseFloat(sessionStorage.getItem("priceWithFormat"));
  }
  protected setSelection(id:number){
    this.selection = id;
  }

  backClicked() {
    this._location.back();
  }

  saveChange(){
    if(this.selection > 0)
    sessionStorage.setItem("finish", this.selection.toString())
    this.finishes.forEach(finish => {
      if(finish.id === this.selection)
        sessionStorage.setItem("priceWithFinish", (this.itemPrice * (finish.percentagePriceChange / 100)).toString())
    })
  }
}
