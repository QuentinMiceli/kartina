import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyingPathComponent } from './buying-path.component';

describe('BuyingPathComponent', () => {
  let component: BuyingPathComponent;
  let fixture: ComponentFixture<BuyingPathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyingPathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyingPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
