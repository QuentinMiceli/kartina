import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ArticlesService } from 'src/Services/articles.service';
import { Articles } from '../../Models/articles';

@Component({
  selector: 'app-buying-path',
  templateUrl: './buying-path.component.html',
  styleUrls: ['./buying-path.component.scss']
})
export class BuyingPathComponent implements OnInit {
  private id:number;
  private currentArticle:Articles;

  constructor(private router:Router, private route:ActivatedRoute, private articlesService:ArticlesService) {
    this.id = this.route.snapshot.params.id;
    this.articlesService.getArticlesById(this.id).subscribe(
      article => {this.currentArticle = article; sessionStorage.setItem("price", this.currentArticle.price.toString())},
      error => console.log(error.message))
    sessionStorage.setItem("article", this.id.toString())
  }

  ngOnInit() {

  }

}
