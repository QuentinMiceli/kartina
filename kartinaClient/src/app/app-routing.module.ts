import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PhotosComponent } from './photos/photos.component';

import { ArtistsComponent } from './artists/artists.component';
import { ArtistDetailComponent } from './artist-detail/artist-detail.component';

import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';

import { AccountComponent } from './account/account.component';
import { AccountDataComponent } from './account/account-data/account-data.component';
import { OrdersComponent } from './account/orders/orders.component';
import { AddressComponent } from './account/address/address.component';
import { AdminSellersComponent } from './account/admin-sellers/admin-sellers.component';
import { AdminOrdersComponent } from './account/admin-orders/admin-orders.component';

import { HelpComponent } from './help/help.component';

import { BuyingPathComponent } from './buying-path/buying-path.component';
import { FormatComponent } from './buying-path/format/format.component';
import { FinishComponent } from './buying-path/finish/finish.component';
import { FrameComponent } from './buying-path/frame/frame.component';

import { CartComponent } from './cart/cart.component';

import { LegalMentionsComponent } from './legal-mentions/legal-mentions.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ArtistDataComponent } from './account/artist-data/artist-data.component';
import { ArtistPhotosComponent } from './account/artist-photos/artist-photos.component';
import { ArtistsPhotosSoldComponent } from './account/artist-photos/artists-photos-sold/artists-photos-sold.component';
import { ArtistsPhotosSellingComponent } from './account/artist-photos/artists-photos-selling/artists-photos-selling.component';
import { ArtistsPhotosAddComponent } from './account/artist-photos/artists-photos-add/artists-photos-add.component';

const routes: Routes = [

  /* Route par défaut */
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
    data: {
      animation: 'home'
    }
  },

  /* Route du catalogue des photos */
  {
    path: 'photos',
    component: PhotosComponent,
    data: {
      animation: 'photos',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Photographies ', url: '' }
      ]
    }
  },

  /* Route amenant vers le détail de la photo et le chemin d'achat */
  {
    path: 'photos/:id',
    component: BuyingPathComponent,
    data: {
      animation: 'photosDetail',
    },
    children: [
      { path: '', redirectTo: 'format', pathMatch: 'full' },
      {
        path: 'format',
        component: FormatComponent,
        data: {
          animation: 'format',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Photographies ', url: '/photos' },
            { label: 'Détail', url: '' }
          ]
        }
      },
      {
        path: 'finish',
        component: FinishComponent,
        data: {
          animation: 'finish',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Photographies ', url: '/photos' },
            { label: 'Détail', url: '' }
          ]
        }
      },
      {
        path: 'frame',
        component: FrameComponent,
        data: {
          animation: 'frame',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Photographies ', url: '/photos' },
            { label: 'Détail', url: '' }
          ]
        }
      }
    ]
  },

  /* Route amenant à la liste des artistes */
  {
    path: 'artists',
    component: ArtistsComponent,
    data: {
      animation: 'artists',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Artistes ', url: '' }
      ]
    }
  },

  /* Route amenant vers le détail d'un artiste */
  {
    path: 'artists/:id',
    component: ArtistDetailComponent,
    data: {
      animation: 'artistsDetail',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Artistes', url: '/artists' },
        { label: 'Détail', url: '' }
      ]
    }
  },

  /* Route amenant vers les pages de login et de création de compte */
  {
    path: 'auth',
    component: AuthComponent,
    data: {
      animation: 'auth',
    },
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      {
        path: 'login',
        component: LoginComponent,
        data: {
          animation: 'login',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Connexion ', url: '' }
          ]
        }
      },
      {
        path: 'register',
        component: RegisterComponent,
        data: {
          animation: 'register',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Connexion ', url: 'auth/login' },
            { label: 'Nouveau Compte ', url: '' }
          ]
        }
      }
    ]
  },

  /* Routes amenant vers les options de gestion de compte */
  {
    path: 'account',
    component: AccountComponent,
    data: {
      animation: 'account',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Mon Compte ', url: '' }
      ]
    },
    children: [
      { path: '', redirectTo: 'data', pathMatch: 'full' },
      {
        path: 'data',
        component: AccountDataComponent,
        data: {
          animation: 'data',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte ', url: '/account' },
            { label: 'Données ', url: '' }
          ]
        }
      },
      {
        path: 'orders',
        component: OrdersComponent,
        data: {
          animation: 'orders',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte ', url: '/account' },
            { label: 'Commandes ', url: '' }
          ]
        }
      },
      {
        path: 'address',
        component: AddressComponent,
        data: {
          animation: 'address',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte ', url: '/account' },
            { label: 'Adresses ', url: '' }
          ]
        }
      },
      {
        path: 'admin-sellers',
        component: AdminSellersComponent,
        data: {
          animation: 'adminSellers',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte', url: '/account' },
            { label: 'Comptes Vendeurs', url: '' }
          ]
        }
      },
      {
        path: 'admin-orders',
        component: AdminOrdersComponent,
        data: {
          animation: 'adminOrders',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte', url: '/account' },
            { label: 'Suivi Commandes', url: '' }
          ]
        }
      },
      {
        path: 'artist-data',
        component: ArtistDataComponent,
        data: {
          animation: 'artistData',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte', url: '/account' },
            { label: 'Données Artiste', url: '' }
          ]
        }
      },
      {
        path: 'artist-photos',
        component: ArtistPhotosComponent,
        data: {
          animation: 'artistPhotos',
          breadcrumb: [
            { label: 'Accueil', url: '/' },
            { label: 'Mon Compte', url: '/account' },
            { label: 'Mes Photos', url: '' }
          ]
        },
        children: [
          { path: '', redirectTo: 'artists-photos-add', pathMatch: 'full' },
          {
            path: 'artists-photos-add',
            component: ArtistsPhotosAddComponent,
            data: {
              animation: 'artistPhotosAdd',
              breadcrumb: [
                { label: 'Accueil', url: '/' },
                { label: 'Mon Compte', url: '/account' },
                { label: 'Mes Photos', url: '' }
              ]
            }
          },
          {
            path: 'artists-photos-selling',
            component: ArtistsPhotosSellingComponent,
            data: {
              animation: 'artistPhotosSelling',
              breadcrumb: [
                { label: 'Accueil', url: '/' },
                { label: 'Mon Compte', url: '/account' },
                { label: 'Mes Photos', url: '' }
              ]
            }
          },
          {
            path: 'artists-photos-sold',
            component: ArtistsPhotosSoldComponent,
            data: {
              animation: 'artistPhotosSold',
              breadcrumb: [
                { label: 'Accueil', url: '/' },
                { label: 'Mon Compte', url: '/account' },
                { label: 'Mes Photos', url: '' }
              ]
            }
          },
        ]
      },
    ]
  },

  /* Route amenant vers la page d'aide */
  {
    path: 'help',
    component: HelpComponent,
    data: {
      animation: 'help',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Aide', url: '' }
      ]
    }
  },

  /* Route amenant vers le panier */
  {
    path: 'cart',
    component: CartComponent,
    data: {
      animation: 'cart',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Panier ', url: '' }
      ]
    }
  },

  /* Route des mentions légales */
  {
    path: 'legal-mentions',
    component: LegalMentionsComponent,
    data: {
      animation: 'legalMentions',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Mentions Légales ', url: '' }
      ]
    }
  },

  /* Route des conditions générales d'utilisation */
  {
    path: 'conditions',
    component: ConditionsComponent,
    data: {
      animation: 'conditions',
      breadcrumb: [
        { label: 'Accueil', url: '/' },
        { label: 'Conditions ', url: '' }
      ]
    }
  },

  /* Route redirigeant vers la page 404 quand l'url n'existe pas */
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }