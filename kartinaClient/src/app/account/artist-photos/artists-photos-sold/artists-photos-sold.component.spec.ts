import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistsPhotosSoldComponent } from './artists-photos-sold.component';

describe('ArtistsPhotosSoldComponent', () => {
  let component: ArtistsPhotosSoldComponent;
  let fixture: ComponentFixture<ArtistsPhotosSoldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistsPhotosSoldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistsPhotosSoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
