import { Component, OnInit } from '@angular/core';
import { ArtistesService } from 'src/Services/artistes.service';
import { Artistes } from '../../Models/artistes';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: ['./artists.component.scss']
})
export class ArtistsComponent implements OnInit {
  private artistes: Artistes[];

  constructor(private artistesService: ArtistesService) {
    this.artistes = new Array();
  }

  ngOnInit() {
    this.artistesService.getArtistes().subscribe(
      (artistes: Artistes[]) => this.artistes = artistes,
      error => console.log(error.message),
    )
  }

}
