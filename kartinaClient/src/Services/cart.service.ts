import { Injectable } from '@angular/core';
import { EventEmitter } from 'events';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private subject = new Subject<any>();

  cartChanged() {
      this.subject.next();
  }

  getCartChanged():Observable<any>{
    return this.subject.asObservable();
  }
}
