import { Injectable } from '@angular/core';
import { Articles } from '../Models/articles';
import { ArticlesReturn } from '../Models/articles-return';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private httpClient:HttpClient) { }

  apiUrl:string = "https://localhost:44314/api/articles";
  apiUrl2:string = "https://localhost:44314/api/articles/topsales";
  apiUrl3:string = "https://localhost:44314/api/articles/newarticles";
  apiUrl4:string = "https://localhost:44314/api/articles/artistarticles";

  getArticles(orientation:string, minPrice:number, maxPrice:number, themeId:number, page:number):Observable<ArticlesReturn>{
    return this.httpClient.get<ArticlesReturn>(this.apiUrl, {
      params: {
        orientation: orientation,
        priceMax: maxPrice.toString(),
        priceMin: minPrice.toString(),
        themeId: themeId.toString(),
        page: page.toString()
      }
    });
  }

  getArticlesById(id:number){
    return this.httpClient.get<Articles>(this.apiUrl + '/' + id)
  }

  getArticlesHome():Observable<Articles[]>{
    return this.httpClient.get<Articles[]>(this.apiUrl2)
  }

  getArticlesCarousel():Observable<Articles[]>{
    return this.httpClient.get<Articles[]>(this.apiUrl3)
  }

  getArticlesByArtiste(artisteId:number){
    return this.httpClient.get<Articles[]>(this.apiUrl4 + '/' + artisteId)
  }

}
