import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Filter } from '../Models/filter'

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private httpClient:HttpClient) { }

  getThemes():Observable<Filter>{
    return this.httpClient.get<Filter>("https://localhost:44314/api/themes");
  }
}
