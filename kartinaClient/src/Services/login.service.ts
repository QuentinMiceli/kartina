import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CurrentUser } from '../Models/current-user';
import { AuthentificationInfos } from '../Models/authentification-infos';
import { catchError } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private subject = new Subject<any>();
  constructor(private httpClient:HttpClient) { }
  apiUrl:string = "https://localhost:44314";

  login(AuthentificationInfos):Observable<CurrentUser>{
      return this.httpClient.post<CurrentUser>(this.apiUrl + "/api/users/authenticate", AuthentificationInfos)
  }

  userChanged(){
    this.subject.next();
  }

  getUserChanged():Observable<any>{
    return this.subject.asObservable();
  }
}
