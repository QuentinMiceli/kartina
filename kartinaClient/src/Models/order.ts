import { OrderLine } from './order-line';

export class Order {
    public id:number;
    public orderedArticles:OrderLine[];

    /**
     *
     */
    constructor(lines:OrderLine[]) {
        this.orderedArticles = lines;
    }
    
}
