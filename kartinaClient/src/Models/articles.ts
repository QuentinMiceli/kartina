export class Articles {
    id:number;
    name:string;
    stock:number;
    imageUrl:string;
    tags:string;
    additionDate:Date;
    frDescription:string;
    enDescription:string;
    price:number;
    themeId:number;
    artisteId:number;
}
