export class Finish {
    public id:number;
    public frName:string;
    public enName:string;
    public frDescrition:string;
    public enDescription:string;
    public percentagePriceChange:number;
    public imageUrl:string;
    public formatId:number;
}
