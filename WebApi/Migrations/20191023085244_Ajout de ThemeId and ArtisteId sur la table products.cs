﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApi.Migrations
{
    public partial class AjoutdeThemeIdandArtisteIdsurlatableproducts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ArtisteId",
                table: "Articles",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles",
                column: "ArtisteId",
                principalTable: "Artists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                table: "Articles",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ArtisteId",
                table: "Articles",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Artists_ArtisteId",
                table: "Articles",
                column: "ArtisteId",
                principalTable: "Artists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
