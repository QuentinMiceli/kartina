using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;
using WebApi.Helpers;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly KartinaContext _context;

        public ArticlesController(KartinaContext context)
        {
            _context = context;
        }

        // GET: api/Articles
        [HttpGet]
        public ActionResult<ArticlesSearchReturnType> GetArticles(string orientation, double? priceMax, double priceMin = 0, int themeId = 0, int page = 1)
        {
            IQueryable<Article> query = _context.Articles.Include(article => article.Artiste);

            if (orientation != null && orientation != "null")
                 query = query.Where(artiste => artiste.Orientation == orientation);

            if (themeId > 0)
                query = query.Where(artiste => artiste.ThemeId == themeId);

            if (priceMax != null && priceMax != 0)
                query = query.Where(article => article.Price >= priceMin && article.Price <= priceMax);

            if (page < 1)
                page = 1;

            return new ArticlesSearchReturnType(query.OrderByDescending(article => article.AdditionDate).Skip((page - 1) * 10).Take(10).ToList(), query.Count());
        }

        public ActionResult<IEnumerable<Article>> GetArticlesByOrientation(string orientation)
        {
            IEnumerable <Article> articles = new List<Article>() { new Article() { Name="test", Orientation="paysage"} };
            return articles.Where(article => article.Orientation.Equals(orientation)).ToList();
        }

        // GET: api/Articles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Article>> GetArticle(int id)
        {
            var article = await _context.Articles.Include(article => article.Artiste).FirstAsync(article => article.Id == id);

            if (article == null)
            {
                return NotFound();
            }

            return article;
        }

        [Route("artistarticles/{id}")]
        public async Task<ActionResult<IEnumerable<Article>>> GetArticleByArtiste(int id)
        {
            return await _context.Articles.Where(article => article.ArtisteId.Equals(id)).ToListAsync();
        }

        [HttpGet]
        [Route("topsales")]
        public async Task<ActionResult<IEnumerable<Article>>> TopSales()
        {
            return await _context.Articles.Include(article => article.Artiste).OrderByDescending(article => article.NbSales).Take(8).ToListAsync();
        }

        [HttpGet]
        [Route("newarticles")]
        public async Task<ActionResult<IEnumerable<Article>>> NewArticles()
        {
            return await _context.Articles.OrderByDescending(article => article.AdditionDate).Take(3).ToListAsync();
        }

        // PUT: api/Articles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArticle(int id, Article article)
        {
            if (id != article.Id)
            {
                return BadRequest();
            }

            _context.Entry(article).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Articles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Article>> PostArticle(Article article)
        {
            _context.Articles.Add(article);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetArticle", new { id = article.Id }, article);
        }

        // DELETE: api/Articles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Article>> DeleteArticle(int id)
        {
            var article = await _context.Articles.FindAsync(id);
            if (article == null)
            {
                return NotFound();
            }

            _context.Articles.Remove(article);
            await _context.SaveChangesAsync();

            return article;
        }

        private bool ArticleExists(int id)
        {
            return _context.Articles.Any(e => e.Id == id);
        }
    }
}
