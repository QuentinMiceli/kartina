using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderedArticlesController : ControllerBase
    {
        private readonly KartinaContext _context;

        public OrderedArticlesController(KartinaContext context)
        {
            _context = context;
        }

        // GET: api/OrderedArticles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderedArticle>>> GetOrderedArticles()
        {
            return await _context.OrderedArticles.ToListAsync();
        }

        // GET: api/OrderedArticles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderedArticle>> GetOrderedArticle(int id)
        {
            var orderedArticle = await _context.OrderedArticles.FindAsync(id);

            if (orderedArticle == null)
            {
                return NotFound();
            }

            return orderedArticle;
        }

        // PUT: api/OrderedArticles/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderedArticle(int id, OrderedArticle orderedArticle)
        {
            if (id != orderedArticle.ArticleId)
            {
                return BadRequest();
            }

            _context.Entry(orderedArticle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderedArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/OrderedArticles
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<OrderedArticle>> PostOrderedArticle(OrderedArticle orderedArticle)
        {
            _context.OrderedArticles.Add(orderedArticle);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (OrderedArticleExists(orderedArticle.ArticleId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetOrderedArticle", new { id = orderedArticle.ArticleId }, orderedArticle);
        }

        // DELETE: api/OrderedArticles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<OrderedArticle>> DeleteOrderedArticle(int id)
        {
            var orderedArticle = await _context.OrderedArticles.FindAsync(id);
            if (orderedArticle == null)
            {
                return NotFound();
            }

            _context.OrderedArticles.Remove(orderedArticle);
            await _context.SaveChangesAsync();

            return orderedArticle;
        }

        private bool OrderedArticleExists(int id)
        {
            return _context.OrderedArticles.Any(e => e.ArticleId == id);
        }
    }
}
