﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class User
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Gender { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string FirstName { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Password { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Profile { get; set; }
        [Column(TypeName = "varchar(15)")]
        public string PhoneNumber { get; set; }
        public List<Address> Adresses { get; set; }
        public List<Order> Orders { get; set; }
        public List<PaymentDetails> PaymentDetails { get; set; }
        public List<Gallery> Galleries { get; set; }

    }
}
