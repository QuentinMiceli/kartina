﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApi.Models
{
    public class Article
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Name { get; set; }
        public int Stock { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string ImageUrl { get; set; }
        public string Tags { get; set; }
        public DateTime AdditionDate { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string Orientation { get; set; }
        public double SpecialOffer { get; set; }
        public string FrDescription { get; set; }
        public string EnDescription { get; set; }
        public double Price { get; set; }
        public int NbSales { get; set; }
        public int? ThemeId { get; set; }
        public Theme Theme {get; set;}
        public int? ArtisteId { get; set; }
        public Artiste Artiste { get; set; }
    }
}
