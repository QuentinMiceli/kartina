﻿namespace WebApi.Models
{
    public class Finish
    {
        public int Id { get; set; }
        public string FrName { get; set; }
        public string EnName { get; set; }
        public string FrDescription { get; set; }
        public string EnDescription { get; set; }
        public double PercentagePriceChange{ get; set; }
        public string ImageUrl { get; set; }
        public int FormatId { get; set; }

    }
}
