﻿namespace WebApi.Models
{
    public class Format
    {
        public int Id { get; set; }
        public string FrName { get; set; }
        public string EnName { get; set; }
        public string Description { get; set; }
        public string Size { get; set; }
        public double PercentagePriceChange { get; set; }
        public string ImageUrl { get; set; } 
 
    }
}
