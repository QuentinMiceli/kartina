﻿using System.ComponentModel.DataAnnotations.Schema;
namespace WebApi.Models
{
    public class Artiste
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string FrPresentation { get; set; }
        public string EnPresentation{ get; set; }
        public string ProEmail { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Printerest { get; set; }
        public string WebSite { get; set; }
        public string TableName { get; set; }
        public string AvatarURL { get; set; }
        public User User { get; set; }
    }
}
