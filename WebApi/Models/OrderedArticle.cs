﻿namespace WebApi.Models
{
    public class OrderedArticle
    {
        public int ArticleId { get; set; }
        public Article Article { get; set; }
        public int FormatId { get; set; }
        public Format Format { get; set; }    
        public int FinishId { get; set; }
        public Finish Finish { get; set; }
        public int FrameId { get; set; }
        public Frame Frame { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Tva { get; set; }
    }
}
