﻿using System;
using System.Collections.Generic;
namespace WebApi.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public string Delivery { get; set; }
        public double DeliveryCosts { get; set; }
        public string AddresStreet { get; set; }
        public string AddressNumber { get; set; }
        public string AddressPostalCode { get; set; }
        public string AddressCity { get; set; }
        public string AdressCountry { get; set; }
        public string Status { get; set; }
        public Gallery Gallery {get; set;}
        public int UserId { get; set; }
        public User User {get; set;}
        public List<OrderedArticle> OrderedArticles { get; set; }

    }
}
