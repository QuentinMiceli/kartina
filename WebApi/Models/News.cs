﻿using System;
namespace WebApi.Models
{
    public class News
    {
        public int Id{ get; set; }
        public string FrTitle { get; set; }
        public string EnTitle { get; set; }
        public string FrText { get; set; }
        public string EnText { get; set; }
        public string Picture { get; set; }
        public DateTime Date { get; set; }
    }
}
