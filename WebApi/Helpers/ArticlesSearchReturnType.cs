﻿using System;
using System.Collections.Generic;
using WebApi.Models;
namespace WebApi.Helpers
{
    public class ArticlesSearchReturnType
    {
        public IList<Article> Articles { get; set; }
        public int NumberOfArticles { get; set; }

        public ArticlesSearchReturnType(IList<Article> articles, int numberOfArticles)
        {
            Articles = articles;
            NumberOfArticles = numberOfArticles;
        }
    }
}
